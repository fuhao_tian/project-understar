"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      touchNum: 0,
      chart_title: "各月订单数据可视化",
      chartData: {
        categories: ["6月", "7月", "8月", "9月", "10月", "11月"],
        series: [
          {
            name: "省内订单数",
            data: [35, 36, 31, 33, 13, 34]
          },
          {
            name: "省外订单数",
            data: [18, 27, 21, 24, 6, 28]
          }
        ]
      },
      artic1: {
        title: "30万亩！央企“种地”！",
        author: "王璟 非常农机",
        get: 151,
        time: "2024-12-11",
        img_url: "https://t8.baidu.com/it/u=2281036363,1606059882&fm=3035&app=3035&size=f242,162&n=0&g=0n&f=JPEG?s=B0193A764551506708E3876C0300F07B&sec=1734161016&t=ced82431b13e9fc5287a6acd90288fdd"
      },
      artic2: {
        title: "勾芡的“芡”是什么？了解价廉物美的“水中人参”",
        author: "光明网",
        get: 291,
        time: "2024-11-21",
        img_url: "https://img2.baidu.com/it/u=2660109569,752045028&fm=253&fmt=auto&app=120&f=JPEG?w=796&h=500"
      },
      artic3: {
        title: "国家粮油信息中心：玉米原料需求形势良好，加工出口形势较为乐观",
        author: "光明网",
        get: 231,
        time: "2024-12-06",
        img_url: "https://img0.baidu.com/it/u=192606279,2468481279&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"
      }
    };
  },
  methods: {
    getServerData() {
      common_vendor.index.request({
        //发动请求从数据库获取数据
        url: "http://127.0.0.1:5000/api/msg",
        method: "POST",
        timeout: 3e3,
        header: {},
        data: "你好！",
        success: (res) => {
          console.log(res.data);
        },
        fail: (res) => {
          console.log("false!");
        }
      });
    },
    updata() {
      common_vendor.index.request({
        url: "",
        method: "POST",
        timeout: 3e3,
        data: {
          time: "2024-12-11"
        },
        success: (res) => {
          this.chartData = res.data.chartData;
          this.chart_title = res.data.chart_title;
          this.artic1 = res.data.artic1;
          this.artic2 = res.data.artic2;
          this.artic3 = res.data.artic3;
        },
        fail: (res) => {
          console.log("false!");
        }
      });
    },
    toartic(datas) {
      common_vendor.index.navigateTo({
        url: "/pages/artic/artic?data=" + datas,
        events: {
          acceptDataFromOpenedPage: function(data) {
            console.log(data);
          }
        },
        success: function(res) {
          res.eventChannel.emit("acceptDataFromOpenerPage", { data: datas });
        },
        fail: function() {
          console.log("文章页面数据传输失败！");
        }
      });
    },
    todeal(e) {
      this.touchNum++;
      setTimeout(() => {
        if (this.touchNum >= 2) {
          common_vendor.index.navigateTo({
            url: "/pages/data_deal/data_deal?data=进入数据分析页面"
          });
        }
        this.touchNum = 0;
      }, 250);
    }
  },
  onReady() {
    this.getServerData();
  }
};
if (!Array) {
  const _easycom_Header2 = common_vendor.resolveComponent("Header");
  const _component_title = common_vendor.resolveComponent("title");
  const _easycom_qiun_data_charts2 = common_vendor.resolveComponent("qiun-data-charts");
  (_easycom_Header2 + _component_title + _easycom_qiun_data_charts2)();
}
const _easycom_Header = () => "../../components/Header/Header.js";
const _easycom_qiun_data_charts = () => "../../uni_modules/qiun-data-charts/components/qiun-data-charts/qiun-data-charts.js";
if (!Math) {
  (_easycom_Header + _easycom_qiun_data_charts)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($data.artic1.title),
    b: common_vendor.t($data.artic1.author + " " + $data.artic1.time),
    c: common_vendor.t($data.artic1.get + " 收藏量"),
    d: $data.artic1.img_url,
    e: common_vendor.o(($event) => $options.toartic("推文1进入")),
    f: common_vendor.t($data.artic2.title),
    g: common_vendor.t($data.artic2.author + " " + $data.artic2.time),
    h: common_vendor.t($data.artic2.get + " 收藏量"),
    i: $data.artic2.img_url,
    j: common_vendor.o(($event) => $options.toartic("推文2进入")),
    k: common_vendor.t($data.artic3.title),
    l: common_vendor.t($data.artic3.author + " " + $data.artic3.time),
    m: common_vendor.t($data.artic3.get + " 收藏量"),
    n: $data.artic3.img_url,
    o: common_vendor.o(($event) => $options.toartic("推文3进入")),
    p: common_vendor.t($data.chart_title),
    q: common_vendor.p({
      type: "column",
      chartData: $data.chartData
    }),
    r: common_vendor.o((...args) => $options.todeal && $options.todeal(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
