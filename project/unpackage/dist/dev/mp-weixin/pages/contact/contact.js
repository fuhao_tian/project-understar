"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    onclick_database() {
      common_vendor.index.navigateTo({
        url: "/pages/client_database/client_database"
      });
    },
    onclick2() {
      common_vendor.index.navigateTo({
        url: "/pages/client_classification/client_classification"
      });
    },
    onclick3() {
      common_vendor.index.navigateTo({
        url: "/pages/new_client/new_client"
      });
    },
    onclick4() {
      common_vendor.index.navigateTo({
        url: "/pages/client_needs/client_needs"
      });
    }
  }
};
if (!Array) {
  const _easycom_Header2 = common_vendor.resolveComponent("Header");
  _easycom_Header2();
}
const _easycom_Header = () => "../../components/Header/Header.js";
if (!Math) {
  _easycom_Header();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.onclick_database && $options.onclick_database(...args)),
    b: common_vendor.o((...args) => $options.onclick2 && $options.onclick2(...args)),
    c: common_vendor.o((...args) => $options.onclick3 && $options.onclick3(...args)),
    d: common_vendor.o((...args) => $options.onclick4 && $options.onclick4(...args)),
    e: common_vendor.o(() => {
    }),
    f: common_vendor.o(() => {
    }),
    g: common_vendor.o(() => {
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
