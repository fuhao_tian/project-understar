"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      key: "",
      title: "",
      autor: "",
      time: "",
      artics: []
    };
  },
  methods: {},
  onLoad(option) {
    const eventChannel = this.getOpenerEventChannel();
    eventChannel.on("acceptDataFromOpenerPage", function(data) {
      console.log(data.data);
    });
    this.key = option.data;
    if (this.key === "推文1进入") {
      console.log("渲染中");
      this.title = "30万亩！央企“种地”！";
      this.autor = "王瑾";
      this.time = "2024年12月11日 06:28";
      this.artics = [
        {
          "contain": "72台农机具、400吨复合肥、203吨小麦种子……齐鲁大地上一场现代化、机械化8000亩小麦冬种生产，搅动着农业领域神经。\n\n这是中铁十四局首个规模化种植现代农业项目的首年度冬季种植。\n\n这是中铁十四局首个规模化种植现代农业项目的首年度冬季种植。\n\n作为世界500强中国铁建所属企业，中铁十四局总部位于山东省济南市，前身为铁道兵第四师，成立于1948年。这位“基建狂魔”成员为进军农业了！背后有何大棋？\n\n",
          "img_choose": true,
          "img_url": "https://mmbiz.qpic.cn/sz_mmbiz_png/gB9bZAXZ3ibOql08luA1kHOjQ0Z0eGYpiaSBgibQuILXUg8d3A5a0ib0S9rCRBUavG5CqPoibvTiaOiame7xlzEic2K3Ng/640?wx_fmt=png&from=appmsg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1",
          "title_choose": true,
          "title": '"耕种管收销":种地有一套'
        },
        {
          "contain": "11月19日至23日，在山东省东营市垦利区杨庙社区以东的这片1.45万亩土地上，35台旋耕机，16台播种机，21台撒肥机、倒运车、还田机同步展开作业，8000亩冬小麦正在种植。\n\n这1.45万亩土地是中铁十四局西北公司通过竞价获得的山东省东营市垦利区山东华澳大地土地经营权。项目地处黄河三角洲的顶点所在地，土地性质为基本农田，其中包括旱地约8000亩，其余为水田、沟渠等。主要种植玉米、小麦、水稻等作物。\n\n中铁十四局方面介绍，受季节影响，8000多亩旱田的小麦种植须在5-7天内完成，才能保次年产出。大片面积、极短时间，给项目组织、农机调度、现场管理带来极大考验。\n\n",
          "img_choose": true,
          "img_url": "https://mmbiz.qpic.cn/sz_mmbiz_png/gB9bZAXZ3ibOql08luA1kHOjQ0Z0eGYpiaVDb3tz0x5b5Qk9NjnicCBNicQ1vAiaHs3p4OpFJIJDfic2kk6ba4HQNE8w/640?wx_fmt=png&from=appmsg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "为了抢抓冬种保产出，项目仅用了3天时间即选定高产小麦品种，集结72台农资机械，400吨复合肥、203吨小麦种子快速运输到场，组建近百人的农业人才团队，从撒肥均匀度、旋耕深度、播种深度等各个环节把控播种质量。\n\n本轮播种的冬小麦将于明年6月份收获。明年4月还将种植约3000亩水稻。\n\n项目将采取“耕种管收销”全流程组织管理，与多家科研院所、高校、龙头企业建立了业务合作关系，储备了技术智库团队；与大型粮油贸易加工企业建立合作订单生产；利用自有电商平台“忆农时代”、地方粮食储备等多渠道保障农产品销售。",
          "img_choose": false,
          "img_url": "",
          "title_choose": true,
          "title": "密谋“数智大田”：流转土地30万亩"
        },
        {
          "contain": "事实上，中铁十四局并非农业领域的“新兵”。\n\n近年来，中铁十四局西北公司深耕现代农业产业链，先后实施了高标准农田建设、规模化种植、土地综合治理、盐碱地改良、现代设施农业等多个现代农业项目，已形成完整的“土地整理—育苗育秧—水稻种植—加工包装—产品销售”农业产业链。\n\n早在2022年，中铁十四局就投资改造了吉林乾安土地综合整治项目（重度苏打盐碱地），联合中国科学院东北地理与农业生态研究所成立科技攻关团队，采用“三良一体化”盐碱地高效治理与综合利用技术，让盐碱地变高产田。",
          "img_choose": true,
          "img_url": "https://mmbiz.qpic.cn/sz_mmbiz_png/gB9bZAXZ3ibOql08luA1kHOjQ0Z0eGYpiaf5pa4Nu6bVfejSllnee1RyrlcmSicd0K0IgaWfGencherUB9xPjdmKw/640?wx_fmt=png&from=appmsg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "在乡村振兴方面，2023年，中铁十四局与中国农科院都市农业研究所签署战略合作协议，共同建设高科技农业科产融合示范基地，深化现代化农业合作。\n\n2024年6月，中铁十四局在深圳市深汕特别合作区赤石镇明溪村多个村庄开展乡村振兴试点，联合滋农（福州）生态农业科技发展有限责任公司，在明溪村试验推行“稻鸭共生”生态种养新模式，并由后者提供鸭子养殖技术指导。稻田、鸭子由农户负责种养，再由滋农（福州）生态农业科技发展有限责任公司统一收购销售，形成农业产销一体化模式。\n\n销售方面，中铁十四局创建了“忆农时代”品牌并申请了商标，采用线下树品牌、线上走销量的方式，对陕西及全国多地农特产品进行销售。同时，中铁十四局还组建了直播团队，进行直播带货，团队现有工作人员40人，专业主播10余人。\n\n中铁十四局等企业还出资成立了石河子市铁建汇德智慧农业发展有限公司（注册资本1000万元人民币），业务涵盖生物有机肥料研发、生物农药技术探索、园区管理服务及智能农业管理。在新成立公司中，生物有机肥与生物农药的研发，将与智能化的农事管理系统相结合。\n\n",
          "img_choose": true,
          "img_url": "https://mmbiz.qpic.cn/sz_mmbiz_png/gB9bZAXZ3ibOql08luA1kHOjQ0Z0eGYpiaSBgibQuILXUg8d3A5a0ib0S9rCRBUavG5CqPoibvTiaOiame7xlzEic2K3Ng/640?wx_fmt=png&from=appmsg&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "业内人士指出，农业是弱势产业，复杂性远远高于工业。即使是发达国家，农业增收很大程度上也是靠高额补贴来支撑。中铁十四局进军农业领域的考量是什么，以及究竟能走多远还有待观察。",
          "img_choose": false,
          "img_url": "",
          "title_choose": false,
          "title": ""
        }
      ];
    } else if (this.key === "推文3进入") {
      this.title = "国家粮油信息中心：玉米原料需求形势良好，加工出口形势较为乐观", this.autor = "光明网", this.time = "2024-12-06", this.artics = [
        {
          "contain": "光明网讯 近日，国家粮油信息中心组织开展玉米市场调研和形势分析，重点了解部分头部饲料、养殖和深加工企业的玉米饲用和加工需求情况及走势。受访企业反映，今年以来玉米深加工生产形势整体正常，特别是新粮上市后，原料采购成本降低有利于企业降本增效，行业开工率明显回升，带动玉米原料需求增长。同时，深加工产品出口形势也较为乐观。某大型饲用氨基酸企业年用玉米超过200万吨，企业生产的苏氨酸和赖氨酸作为饲料添加剂，需求量稳中提升，主要用于猪料、禽料、水产等，另外新兴产品如宠物饲料的增速较快；今年前10个月实现加工产品同比增产18%，产品主要出口到欧洲、亚洲和拉美地区。\n\n受访的饲料养殖企业也反映，玉米的原料需求形势较好。某大型农牧食品上市公司表示，其年饲料产量和生猪出栏量均位列国内行业头部，企业今年前三季度营收超700亿元，饲料总销量超过1900万吨。另一饲料上市公司年饲料销量超过2000万吨，今年前三季度面对养殖行情波动，饲料产业竞争日益激烈的行业挑战，企业持续强化科技、服务等核心竞争力，实现营业收入超800亿元，经营业绩稳健增长。饲料和加工企业对于后期的用粮需求较为乐观，并表示将增大采购数量，把库存建立在合理水平。\n\n此外，中储粮集团公司于12月5日发布公告称，其所属相关企业近期将在东北等玉米产区继续增加2024年产国产玉米收储规模，积极入市开展收购。（宋雅娟）\n\n[责编：肖春芳]",
          "img_choose": false,
          "img_url": "",
          "title_choose": false,
          "title": ""
        }
      ];
    } else if (this.key === "推文2进入") {
      this.title = "勾芡的“芡”是什么？了解价廉物美的“水中人参”", this.autor = "光明网", this.time = "2024-11-21", this.artics = [
        {
          "contain": "人们炒菜时，最后一道工序是勾芡，即用淀粉和水调成稠状液体，在菜接近成熟时，浇淋入锅中。淀粉经加热发生糊化吸收汤中的水分，可使菜肴汤汁稠浓而突出主料，菜品质地滑润美观有光泽。这道工序明明用的是淀粉，为何叫“勾芡”？这是因为在古代，用来调制浓稠汁液的淀粉原料起初是从“芡”这种植物的种子中研磨而得的。\n",
          "img_choose": false,
          "img_url": "",
          "title_choose": true,
          "title": "芡是什么？"
        },
        {
          "contain": "芡是睡莲科芡属一年生大型浮水植物，全株具刺，地下有短肥的根茎。叶有沉水和浮水两种，沉水叶小，箭形或椭圆肾形；浮水叶革质，圆形盾状，大可达1米以上，边缘无锯齿。叶表面浓绿色，十分皱曲；背面紫色，网状的带刺叶脉明显隆起。",
          "img_choose": true,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b3404.jpg",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "每年7~8月从叶腋中生出单朵花，由粗壮的长梗（梗上有硬刺）托举出水面；花托（位于花梗顶端的膨大部分，花萼、花瓣、雄蕊、雌蕊依次着生在花托上）多刺，状如鸡头，也像刺猬或板栗果实，因此芡的果实被称为“鸡头米”。花长约5厘米，花萼4枚，外面绿色，密生稍弯硬刺，内面紫色；花瓣多数，紫红色，成数轮排列，向内渐变成雄蕊。芡的花昼开夜闭，其浆果为球形紫红色，直径3~5厘米，外面密生硬刺。种子为黑色球形，直径为1厘米左右。\n\n",
          "img_choose": true,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b3c05.jpg",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "因为芡跟荷花同科，亲缘关系较近，所以芡的种子和莲子非常相似。芡的种子又叫水栗，顾名思义，它的种仁跟板栗一样富含淀粉，碾磨成粉后质地较为黏稠，适合在家庭烹饪中用来煲汤勾芡和做各种副食品，是古代中国很重要的淀粉来源，灾荒年可救援济民。李时珍曰：芡可济俭歉，故谓之芡。\n\n芡按生长方式有南芡和北芡之分：南芡为栽培种，主要在江浙一带栽培，种在土层深厚松软、富含有机质的湖荡土中。植株个体较大，地上器官除叶背有刺外，其余部分均光滑无刺，采收较方便。种子较大，种仁圆整、糯性，品质优良，但适应性和抗逆性较差。北芡为野生种，地上器官密生刚刺，采收较困难，种子较小，质地略次于南芡但适应性较强。\n\n南芡主要作食品并出口，而北芡主要作药用。\n",
          "img_choose": false,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b3c05.jpg",
          "title_choose": true,
          "title": "水中人参"
        },
        {
          "contain": "芡的种子具有很高的强身滋补价值，被称为“水中人参”，更难得的是它其分布广泛，价廉物美。\n\n从黑龙江至云南、广东，芡遍布我国南北，生长在池塘、湖沼中。它长势强健，适应性强，深浅水中均能生长，尤其喜欢温暖、阳光充足的环境，不耐霜冻和干旱，植株入冬前死亡，第二年由种子萌发新株。\n\n芡实的种仁又白又嫩状如鱼目，含有大量不饱和脂肪酸——亚麻酸，具有很高的营养价值，可炒食或做成芡实糕，均美味可口，若煮粥可与银耳相媲美，更是秋季滋补的粗粮佳品。且补而不峻、防燥不腻，尤其适合久病不愈致虚喘的人食用。广东人爱用它做各种煲，与南瓜、百合等配料入砂锅炖得软烂入味。\n\n芡实始载于《神农本草经》，列为延年益寿的上品，历版《中国药典》均有记载。《本草纲目》称芡实粥具有补脾益肾、稳固精气、治疗腰膝酸软等功效，是传统的中药材和珍贵的天然滋补品，被列入药食同源物质名单。",
          "img_choose": false,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b3c05.jpg",
          "title_choose": true,
          "title": "芡的其他应用"
        },
        {
          "contain": "芡叶大如盖，叶片背面清晰可见整齐的脉络结构——粗大的一级叶脉呈辐射状排列，其间又有纵横交叉的次级叶脉，将整张叶片精细地分割成无数个大小相仿的单元，这样如蛛网般合理有序的结构具备优秀的力学性能，给叶片带来良好的支持力和平衡性；芡的叶脉之中还藏有许多气室，像是给叶片配上了量身内置的“救生圈”，赋予漂浮叶片很好的承重能力。因此，芡叶成为难得的水面漂浮平台，给许多水鸟提供了优秀的栖息环境，在生态系统中有着非常重要的作用。\n\n",
          "img_choose": true,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b4506.jpg",
          "title_choose": false,
          "title": ""
        },
        {
          "contain": "芡全草为猪饲料，又可作绿肥。芡的嫩叶柄和花柄剥去皮后可作蔬菜。芡实除了食用，还可酿酒。\n\n芡叶形、花托奇特，花色明丽（有白花品种），用于水面绿化观赏颇有野趣。园林中常植于浅水处，多与荷花、睡莲、香蒲等配植，均具有较高的观赏性。\n\n作者：王珏（北京林业大学副教授）、丁霞（北京市第五十五中学生物教师）\n\n策划：谢芸",
          "img_choose": false,
          "img_url": "https://kepu.gmw.cn/agri/attachement/jpg/site2/20241121/88aedd1c850828a92b4506.jpg",
          "title_choose": false,
          "title": ""
        }
      ];
    }
  }
};
if (!Array) {
  const _component_title = common_vendor.resolveComponent("title");
  _component_title();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.t($data.title),
    b: common_vendor.t($data.autor + " " + $data.time),
    c: common_vendor.f($data.artics, (artic, index, i0) => {
      return common_vendor.e({
        a: common_vendor.t(artic.contain),
        b: artic.img_choose
      }, artic.img_choose ? {
        c: artic.img_url
      } : {}, {
        d: artic.title_choose
      }, artic.title_choose ? {
        e: common_vendor.t(artic.title),
        f: "66192c00-1-" + i0
      } : {}, {
        g: index
      });
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
