"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    onclick1() {
      common_vendor.index.navigateTo({
        url: "/pages/produce/produce"
      });
    },
    onclick2() {
      common_vendor.index.navigateTo({
        url: "/pages/bill_management/bill_management"
      });
    },
    onclick3() {
      common_vendor.index.navigateTo({
        url: "/pages/data_deal/data_deal"
      });
    }
  }
};
if (!Array) {
  const _easycom_Header2 = common_vendor.resolveComponent("Header");
  _easycom_Header2();
}
const _easycom_Header = () => "../../components/Header/Header.js";
if (!Math) {
  _easycom_Header();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => $options.onclick1 && $options.onclick1(...args)),
    b: common_vendor.o((...args) => $options.onclick2 && $options.onclick2(...args)),
    c: common_vendor.o((...args) => $options.onclick3 && $options.onclick3(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
