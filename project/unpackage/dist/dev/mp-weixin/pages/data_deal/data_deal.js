"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      title: "这是一篇标题",
      screenwidth: 0,
      list: [
        { id: 1, title: "订单量" },
        { id: 2, title: "销售额" },
        { id: 3, title: "盈利情况" },
        { id: 4, title: "客户画像" },
        { id: 5, title: "客户需求" },
        { id: 6, title: "员工业绩" }
      ],
      currentIndex: 0,
      order_data: {
        title: "2024年订单情况",
        order_data_main: {
          categories: ["6月", "7月", "8月", "9月", "10月", "11月"],
          series: [
            {
              name: "省内订单数",
              data: [35, 36, 31, 33, 13, 34]
            },
            {
              name: "省外订单数",
              data: [18, 27, 21, 24, 6, 28]
            }
          ]
        },
        opts: {
          extra: {
            column: {
              width: 20,
              activeBgColor: "#D4E4DC"
            }
          },
          xAxis: {
            titleOffsetX: 5
          }
        }
      }
    };
  },
  methods: {
    selectNav(index) {
      this.currentIndex = index;
    }
  },
  onLoad(data) {
    this.screenwidth = common_vendor.index.getWindowInfo().screenWidth - 100;
    console.log(data);
  }
};
if (!Array) {
  const _easycom_qiun_data_charts2 = common_vendor.resolveComponent("qiun-data-charts");
  _easycom_qiun_data_charts2();
}
const _easycom_qiun_data_charts = () => "../../uni_modules/qiun-data-charts/components/qiun-data-charts/qiun-data-charts.js";
if (!Math) {
  _easycom_qiun_data_charts();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return common_vendor.e({
    a: common_vendor.f($data.list, (model, index, i0) => {
      return {
        a: common_vendor.t(model.title),
        b: index,
        c: common_vendor.n($data.currentIndex === index ? "active" : "line"),
        d: common_vendor.o(($event) => $options.selectNav(index), index)
      };
    }),
    b: $data.currentIndex === 0
  }, $data.currentIndex === 0 ? {
    c: common_vendor.t($data.order_data.title),
    d: common_vendor.p({
      type: "column",
      chartData: $data.order_data.order_data_main,
      opts: $data.order_data.opts,
      canvas2d: "true"
    }),
    e: $data.screenwidth + "px"
  } : $data.currentIndex === 1 ? {
    g: common_vendor.t($data.order_data.title),
    h: common_vendor.p({
      type: "column",
      chartData: $data.order_data.order_data_main,
      opts: $data.order_data.opts,
      canvas2d: "true"
    }),
    i: $data.screenwidth + "px"
  } : $data.currentIndex === 2 ? {
    k: common_vendor.t($data.list[$data.currentIndex].title),
    l: $data.screenwidth + "px"
  } : $data.currentIndex === 3 ? {
    n: common_vendor.t($data.list[$data.currentIndex].title),
    o: $data.screenwidth + "px"
  } : $data.currentIndex === 4 ? {
    q: common_vendor.t($data.list[$data.currentIndex].title),
    r: $data.screenwidth + "px"
  } : $data.currentIndex === 5 ? {
    t: common_vendor.t($data.list[$data.currentIndex].title),
    v: $data.screenwidth + "px"
  } : {}, {
    f: $data.currentIndex === 1,
    j: $data.currentIndex === 2,
    m: $data.currentIndex === 3,
    p: $data.currentIndex === 4,
    s: $data.currentIndex === 5
  });
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
