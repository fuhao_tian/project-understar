"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      tableData: [
        { name: "和时间", age: 89, number: 18975246593 },
        { name: "李四", age: 25, number: 15849325597 },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "李四", age: 25, number: "09876543210" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "sm三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "bs三", age: 28, number: "12345678901" },
        { name: "张ssh", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" },
        { name: "张三", age: 28, number: "12345678901" }
      ]
    };
  },
  methods: {}
};
if (!Array) {
  const _easycom_Header2 = common_vendor.resolveComponent("Header");
  const _component_uni_th = common_vendor.resolveComponent("uni-th");
  const _component_uni_tr = common_vendor.resolveComponent("uni-tr");
  const _component_uni_td = common_vendor.resolveComponent("uni-td");
  const _component_uni_table = common_vendor.resolveComponent("uni-table");
  (_easycom_Header2 + _component_uni_th + _component_uni_tr + _component_uni_td + _component_uni_table)();
}
const _easycom_Header = () => "../../components/Header/Header.js";
if (!Math) {
  _easycom_Header();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      align: "left",
      width: "50"
    }),
    b: common_vendor.p({
      align: "center",
      width: "50"
    }),
    c: common_vendor.p({
      align: "center",
      width: "50"
    }),
    d: common_vendor.p({
      align: "center",
      width: "50"
    }),
    e: common_vendor.f($data.tableData, (item, index, i0) => {
      return {
        a: common_vendor.t(index + 1),
        b: "45fa7938-8-" + i0 + "," + ("45fa7938-7-" + i0),
        c: common_vendor.t(item.name),
        d: "45fa7938-9-" + i0 + "," + ("45fa7938-7-" + i0),
        e: common_vendor.t(item.age),
        f: "45fa7938-10-" + i0 + "," + ("45fa7938-7-" + i0),
        g: common_vendor.t(item.number),
        h: "45fa7938-11-" + i0 + "," + ("45fa7938-7-" + i0),
        i: index,
        j: "45fa7938-7-" + i0 + ",45fa7938-1"
      };
    }),
    f: common_vendor.p({
      width: "50"
    }),
    g: common_vendor.p({
      width: "50"
    }),
    h: common_vendor.p({
      width: "50"
    }),
    i: common_vendor.p({
      width: "50"
    }),
    j: common_vendor.sr("table", "45fa7938-1"),
    k: common_vendor.o(_ctx.selectionChange),
    l: common_vendor.p({
      loading: _ctx.loading,
      type: "selection",
      emptyText: "暂无更多数据"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render]]);
wx.createPage(MiniProgramPage);
