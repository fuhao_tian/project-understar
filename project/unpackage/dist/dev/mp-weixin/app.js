"use strict";
Object.defineProperty(exports, Symbol.toStringTag, { value: "Module" });
const common_vendor = require("./common/vendor.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/contact/contact.js";
  "./pages/management/management.js";
  "./pages/produce/produce.js";
  "./pages/bill_management/bill_management.js";
  "./pages/data_deal/data_deal.js";
  "./pages/client_database/client_database.js";
  "./pages/client_classification/client_classification.js";
  "./pages/new_client/new_client.js";
  "./pages/client_needs/client_needs.js";
  "./pages/artic/artic.js";
}
const _sfc_main = {
  onLaunch: function() {
    console.log("App Launch");
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
function createApp() {
  const app = common_vendor.createSSRApp(_sfc_main);
  return {
    app
  };
}
createApp().app.mount("#app");
exports.createApp = createApp;
