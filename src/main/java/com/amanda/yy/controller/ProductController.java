package com.amanda.yy.controller;

import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Product;
import com.amanda.yy.pojo.Result;
import com.amanda.yy.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/yy/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * 产品分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer pageSize){
        log.info("分页查询，参数：page={},pageSize={}",page,pageSize);
        PageResult pageResult = productService.pageQuery(page, pageSize);
        return Result.success(pageResult);
    }

    /**
     * 新增产品
     * @param product
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Product product){
        log.info("新增菜品，菜品数据：{}",product);
        productService.add(product);
        return Result.success();
    }

    /**
     * 根据id删除产品
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id){
        log.info("根据id删除产品，id：{}",id);
        productService.delete(id);
        return Result.success();
    }

    /**
     * 修改产品
     * @param product
     * @return
     */
    @PutMapping
    public Result update(@RequestBody Product product){
        log.info("修改菜品，菜品数据：{}",product);
        productService.update(product);
        return Result.success();
    }
    @GetMapping("/{categoryId}")
    public Result<PageResult> page(@PathVariable Long categoryId,@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pageSize){
        log.info("根据分类id查询菜品");
        PageResult pageResult = productService.pageQueryByCategoryId(categoryId, page, pageSize);
        return Result.success(pageResult);
    }
}
