package com.amanda.yy.controller;

import com.amanda.yy.mapper.EmpMapper;
import com.amanda.yy.pojo.Employee;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Result;
import com.amanda.yy.service.EmpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("yy/employee")
public class EmpController {
    @Autowired
    private EmpService empService;

    /**
     * 分页查询员工资料
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer pageSize){
        log.info("分页查询:{},{}",page,pageSize);
        PageResult pageResult = empService.page(page, pageSize);
        return Result.success(pageResult);
    }

    /**
     * 新增员工
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody Employee employee){
        log.info("新增员工:{}",employee);
        empService.add(employee);
        return Result.success();
    }
}
