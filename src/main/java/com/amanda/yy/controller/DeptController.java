package com.amanda.yy.controller;

import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Result;
import com.amanda.yy.service.DeptService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/yy/dept")
public class DeptController{
    @Autowired
    private DeptService deptService;

    /**
     * 部门分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pageSize){
        log.info("分页查询，参数：page={},pageSize={}",page,pageSize);
        PageResult pageResult = deptService.pageQuery(page, pageSize);
        return Result.success(pageResult);
    }
}
