package com.amanda.yy.controller;

import com.amanda.yy.pojo.Order;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Result;
import com.amanda.yy.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/yy/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 查询所有订单
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page(@RequestParam(defaultValue = "1") Integer page, @RequestParam(defaultValue = "10") Integer pageSize){
        log.info("分页查询，参数：page={},pageSize={}",page,pageSize);
        PageResult pageResult = orderService.pageQuery(page, pageSize);
        return Result.success(pageResult);
    }

    /**
     * 新增订单
     * @param order
     * @return
     */
    @PostMapping
    public Result add(@RequestBody Order order){
        log.info("添加订单:{}",order);
        orderService.add(order);
        return Result.success();
    }
}
