package com.amanda.yy.controller;

import com.amanda.yy.pojo.Client;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Result;
import com.amanda.yy.service.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/yy/client")
public class ClientController {
    @Autowired
    private ClientService clientService;

    /**
     * 客户分页查询
     * @param page
     * @param pageSize
     * @return
     */

    @GetMapping("/page")
    public Result<PageResult> page(@RequestParam(defaultValue = "1") Integer page,@RequestParam(defaultValue = "10") Integer pageSize){
        log.info("客户分页查询:{},{}",page,pageSize);
        PageResult pageResult = clientService.page(page, pageSize);
        return Result.success(pageResult);
    }

    /**
     * 新增客户
     * @param client
     * @return
     */
    @PostMapping
    public Result<String> add(@RequestBody Client client){
        log.info("添加客户:{}",client);
        clientService.add(client);
        return Result.success();
    }

    /**
     *  根据类别分页查询
     * @param categoryId
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/{categoryId}")
    public Result<PageResult> pageByCategory(@PathVariable Integer categoryId,
                                            @RequestParam(defaultValue = "1") Integer page,
                                            @RequestParam(defaultValue = "10") Integer pageSize){
        log.info("根据类别分页查询:{},{},{}",categoryId,page,pageSize);
        PageResult pageResult = clientService.pageByCategory(categoryId, page, pageSize);
        return Result.success(pageResult);
    }
    @GetMapping("/demand")
    public Result<PageResult> demandQuery(@RequestParam(defaultValue = "1") Integer page,
                                          @RequestParam(defaultValue = "10") Integer pageSize){
        log.info("需求查询:{},{}",page,pageSize);
        PageResult pageResult = clientService.demandQuery(page, pageSize);
        return Result.success(pageResult);
    }
}
