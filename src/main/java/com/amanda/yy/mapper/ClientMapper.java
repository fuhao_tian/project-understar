package com.amanda.yy.mapper;

import com.amanda.yy.pojo.Client;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;



@Mapper
public interface ClientMapper {

    @Select("select * from client")
    Page<Client> pageQuery(Integer page, Integer pageSize);

    @Insert("insert into client(name, age, gender, phone, district, category_id, create_time, update_time, demand) value (#{name},#{age},#{gender},#{phone},#{district},#{categoryId},#{createTime},#{updateTime},#{demand})")
    void insert(Client client);
    @Select("select * from client where category_id = #{categoryId}")
    Page<Client> pageQueryByCategoryId(Integer categoryId, Integer page, Integer pageSize);

    @Select("select name,demand from client ")
    Page<Client> demandQuery(Integer page, Integer pageSize);
}
