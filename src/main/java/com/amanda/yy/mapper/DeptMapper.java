package com.amanda.yy.mapper;

import com.amanda.yy.pojo.Dept;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface DeptMapper {
    @Select("select * from dept")
    Page<Dept> pageQuery(Integer page, Integer pageSize);
}
