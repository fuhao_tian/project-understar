package com.amanda.yy.mapper;

import com.amanda.yy.pojo.Product;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.*;

@Mapper
public interface ProductMapper {
    @Select("select * from product")
    Page<Product> pageQuery(Integer page, Integer pageSize);

    @Insert("insert into product(name, category_id, price, image, description, provenance, create_time, update_time) VALUE (#{name},#{categoryId},#{price},#{image},#{description},#{provenance},#{createTime},#{updateTime})")
    void insert(Product product);
    @Delete("delete from product where id = #{id}")
    void delete(Long id);
    @Update("update product set name=#{name},category_id=#{categoryId},price=#{price},image=#{image},description=#{description},provenance=#{provenance} where id = #{id}")
    void update(Product product);
    @Select("select * from product where category_id = #{categoryId}")
    Page<Product> pageQueryByCategoryId(Long categoryId, Integer page, Integer pageSize);



}
