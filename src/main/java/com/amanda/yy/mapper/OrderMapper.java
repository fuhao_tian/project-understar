package com.amanda.yy.mapper;

import com.amanda.yy.pojo.Order;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface OrderMapper {
    @Select("select * from `order`")
    Page<Order> pageQuery(Integer page, Integer pageSize);
    @Insert("insert into `order`(order_number, total_price, delivery_time, remark, phone, address) VALUE(#{orderNumber},#{totalPrice},#{deliveryTime},#{remark},#{phone},#{address}) ")
    void insert(Order order);
}
