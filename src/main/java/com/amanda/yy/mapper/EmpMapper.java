package com.amanda.yy.mapper;

import com.amanda.yy.pojo.Employee;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface EmpMapper {
    @Select("select * from employee")
    Page<Employee> pageQuery(Integer page, Integer pageSize);

    @Insert("insert into employee(name, age, gender, dept_id, phone, entrydate, create_time, update_time) VALUE (#{name},#{age},#{gender},#{deptId},#{phone},#{entrydate},#{createTime},#{updateTime})")
    void insert(Employee employee);
}
