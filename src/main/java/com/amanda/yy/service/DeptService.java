package com.amanda.yy.service;

import com.amanda.yy.pojo.PageResult;

public interface DeptService {

    PageResult pageQuery(Integer page, Integer pageSize);
}
