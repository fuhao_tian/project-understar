package com.amanda.yy.service;

import com.amanda.yy.pojo.Order;
import com.amanda.yy.pojo.PageResult;

public interface OrderService {
    PageResult pageQuery(Integer page, Integer pageSize);

    void add(Order order);
}
