package com.amanda.yy.service;

import com.amanda.yy.pojo.Client;
import com.amanda.yy.pojo.PageResult;

public interface ClientService {
    PageResult page(Integer page, Integer pageSize);

    void add(Client client);

    PageResult pageByCategory(Integer categoryId, Integer page, Integer pageSize);

    PageResult demandQuery(Integer page, Integer pageSize);
}
