package com.amanda.yy.service;

import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Product;

public interface ProductService {
    PageResult pageQuery(Integer page, Integer pageSize);

    void add(Product product);

    void delete(Long id);

    void update(Product product);



    PageResult pageQueryByCategoryId(Long categoryId,Integer page, Integer pageSize);

}
