package com.amanda.yy.service;

import com.amanda.yy.pojo.Employee;
import com.amanda.yy.pojo.PageResult;

public interface EmpService {
    PageResult page(Integer page, Integer pageSize);

    void add(Employee employee);
}
