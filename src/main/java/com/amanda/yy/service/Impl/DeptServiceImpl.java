package com.amanda.yy.service.Impl;

import com.amanda.yy.mapper.DeptMapper;
import com.amanda.yy.pojo.Dept;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.service.DeptService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;

    @Override
    public PageResult pageQuery(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Dept> depts = deptMapper.pageQuery(page, pageSize);
        PageResult pageResult = new PageResult(depts.getTotal(), depts.getResult());
        return pageResult;
    }
}
