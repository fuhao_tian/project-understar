package com.amanda.yy.service.Impl;

import com.amanda.yy.mapper.ProductMapper;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.pojo.Product;
import com.amanda.yy.service.ProductService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductMapper productMapper;
    @Override
    public PageResult pageQuery(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Product> products = productMapper.pageQuery(page,pageSize);
        PageResult pageResult = new PageResult(products.getTotal(), products.getResult());
        return pageResult;
    }

    @Override
    public void add(Product product) {
        product.setCreateTime(LocalDateTime.now());
        product.setUpdateTime(LocalDateTime.now());
        productMapper.insert(product);
    }

    @Override
    public void delete(Long id) {
        productMapper.delete(id);
    }

    @Override
    public void update(Product product) {
        productMapper.update(product);
    }

    @Override
    public PageResult pageQueryByCategoryId(Long categoryId, Integer page, Integer pageSize) {
        PageHelper.startPage(page,pageSize);
        Page<Product> products = productMapper.pageQueryByCategoryId(categoryId,page,pageSize);
        PageResult pageResult = new PageResult(products.getTotal(), products.getResult());
        return pageResult;
    }
}
