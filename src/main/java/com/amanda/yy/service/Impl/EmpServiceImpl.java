package com.amanda.yy.service.Impl;

import com.amanda.yy.mapper.EmpMapper;
import com.amanda.yy.pojo.Employee;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.service.EmpService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EmpServiceImpl implements EmpService {
    @Autowired
    private EmpMapper empMapper;
    @Override
    public PageResult page(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Employee> employees = empMapper.pageQuery(page, pageSize);
        PageResult pageResult = new PageResult(employees.getTotal(), employees.getResult());
        return pageResult;
    }

    @Override
    public void add(Employee employee) {
        employee.setCreateTime(LocalDateTime.now());
        employee.setUpdateTime(LocalDateTime.now());
        empMapper.insert(employee);
    }
}
