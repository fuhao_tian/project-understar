package com.amanda.yy.service.Impl;

import com.amanda.yy.mapper.ClientMapper;
import com.amanda.yy.pojo.Client;
import com.amanda.yy.pojo.PageResult;

import com.amanda.yy.service.ClientService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;



@Service
public class ClientServiceImpl implements ClientService {
    @Autowired
    private ClientMapper clientMapper;
    @Override
    public PageResult page(Integer page, Integer pageSize) {

        PageHelper.startPage(page, pageSize);
        Page<Client> clients = clientMapper.pageQuery(page, pageSize);

        PageResult pageResult = new PageResult(clients.getTotal(), clients.getResult());
        return pageResult;
    }

    @Override
    public void add(Client client) {
        client.setCreateTime(LocalDateTime.now());
        client.setUpdateTime(LocalDateTime.now());
        clientMapper.insert(client);
    }

    @Override
    public PageResult pageByCategory(Integer categoryId, Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Client> clients = clientMapper.pageQueryByCategoryId(categoryId,page, pageSize);
        PageResult pageResult = new PageResult(clients.getTotal(), clients.getResult());
        return pageResult;
    }

    @Override
    public PageResult demandQuery(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Client> clients = clientMapper.demandQuery(page, pageSize);

        PageResult pageResult = new PageResult(clients.getTotal(), clients.getResult());
        return pageResult;    }
}
