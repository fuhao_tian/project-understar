package com.amanda.yy.service.Impl;

import com.amanda.yy.mapper.OrderMapper;
import com.amanda.yy.pojo.Order;
import com.amanda.yy.pojo.PageResult;
import com.amanda.yy.service.OrderService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public PageResult pageQuery(Integer page, Integer pageSize) {
        PageHelper.startPage(page, pageSize);
        Page<Order> orders = orderMapper.pageQuery(page, pageSize);
        PageResult pageResult = new PageResult(orders.getTotal(), orders.getResult());
        return pageResult;
    }

    @Override
    public void add(Order order) {
        order.setCreateTime(LocalDateTime.now());
        orderMapper.insert(order);
    }
}
