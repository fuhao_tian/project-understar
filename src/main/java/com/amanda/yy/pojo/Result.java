package com.amanda.yy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {
    private Integer code;
    private String message;
    private T data;

    //增删改返回数据
    public static <T> Result<T> success(){
        Result<T> result = new Result<>();
        result.setCode(1);

        return result;
    }
    //查询返回数据
    public static <T> Result<T> success(T data){
        Result<T> result = new Result<>();
        result.setCode(1);

        result.setData(data);
        return result;
    }
    //响应失败返回数据
    public static <T> Result<T> error(String message){
        Result<T> result = new Result<>();
        result.setCode(0);
        result.setMessage(message);
        return result;
    }


}
