package com.amanda.yy.pojo;

import lombok.AllArgsConstructor;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Client {
    private Integer id;
    private String name;
    private Integer age;
    private String gender;
    private String phone;
    private String district;
    private Integer categoryId;
    private String demand;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
