package com.amanda.yy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private Integer id;
    private String name;
    private Integer categoryId;
    private BigDecimal price;
    private String image;
    private String description;
    private String provenance;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
