package com.amanda.yy.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.PrimitiveIterator;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private Integer id;
    private String name;
    private Integer age;
    private String gender;
    private Integer deptId;
    private String phone;
    private LocalDate entrydate;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
