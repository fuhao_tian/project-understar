create table client_category
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP
);

create table client
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    age         int                                null,
    gender      enum ('男', '女')                  null,
    phone       varchar(20)                        null,
    district    varchar(255)                       null,
    category_id int                                null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP,
    demand      varchar(255)                       null,
    constraint fk_client_category
        foreign key (category_id) references client_category (id)
);

create table dept
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP
);

create table employee
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    age         int                                null,
    gender      enum ('男', '女')                  null,
    dept_id     int                                null,
    phone       varchar(20)                        null,
    entrydate   date                               null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP,
    constraint fk_employee_dept
        foreign key (dept_id) references dept (id)
);

create table `order`
(
    id            int auto_increment
        primary key,
    order_number  varchar(255)                       not null,
    total_price   decimal(10, 2)                     null,
    delivery_time datetime                           null,
    remark        text                               null,
    phone         varchar(20)                        null,
    address       varchar(255)                       null,
    create_time   datetime default CURRENT_TIMESTAMP null,
    update_time   datetime                           null on update CURRENT_TIMESTAMP
);

create table product_category
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP
);

create table product
(
    id          int auto_increment
        primary key,
    name        varchar(255)                       not null,
    category_id int                                null,
    price       decimal(10, 2)                     null,
    image       varchar(255)                       null,
    description text                               null,
    provenance  varchar(255)                       null,
    create_time datetime default CURRENT_TIMESTAMP null,
    update_time datetime                           null on update CURRENT_TIMESTAMP,
    constraint fk_product_category
        foreign key (category_id) references product_category (id)
);


